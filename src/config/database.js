// Config MySQL
const mysql = require("mysql");
const database = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
  database: "bootcamp-loyalty-point",
});
database.connect((err) => {
  if (err) throw err;
});

module.exports = database;
