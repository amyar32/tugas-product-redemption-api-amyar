const express = require("express");
const bodyParser = require("body-parser");
const app = express();

// Config Body Parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

module.exports = app;
