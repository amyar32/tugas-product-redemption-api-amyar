const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  const authHeader = req.get("Authorization");

  if (!authHeader) {
    res.status(401).json({
      message: "Unauthorized",
    });
    return;
  }

  try {
    const token = authHeader.split(" ")[1];
    jwt.verify(token, "secret");
  } catch (err) {
    res.status(500).json({
      message: "Unauthorized",
    });
    console.log(err);
  }

  next();
};
