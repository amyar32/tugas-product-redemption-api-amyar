const express = require("express");
const auth = require("../middleware/auth");
const productValidationRules = require("../modules/product/validation/productValidationRules");
const { validate } = require("../config/validator.js");
const productController = require("../modules/product/controller/productController");
const route = express.Router();

route.get("/", productController.getAllProduct);
route.post("/", auth, productValidationRules(), validate, productController.createProduct);
route.get("/:id", productController.getDetailProduct);
route.put("/:id", auth, productController.updateProduct);
route.delete("/:id", auth, productController.deleteProduct);

module.exports = route;
