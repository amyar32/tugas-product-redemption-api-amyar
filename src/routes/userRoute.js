const express = require("express");
const auth = require("../middleware/auth");
const userValidationRules = require("../modules/user/validation/userValidationRules");
const { validate } = require("../config/validator.js");
const userController = require("../modules/user/controller/userController");
const route = express.Router();

route.get("/", userController.getAllUser);
route.post("/", userValidationRules(), validate, userController.createUser);
route.get("/:id", userController.getDetailUser);
route.put("/:id", auth, userController.updateUser);
route.delete("/:id", auth, userController.deleteUser);

module.exports = route;
