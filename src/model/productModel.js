const database = require("../config/database");

const productModel = {
    async getAllProduct(order = "id.asc", limit = 5, page = 1) {
        return new Promise((resolve, reject) => {
            let query = "SELECT * FROM products ";

            if (order) {
                const orderParam = order.split(".");
                query += `ORDER BY ${orderParam[0]} ${orderParam[1]} `;
            }

            if (page && limit) {
                query += `LIMIT ${(page - 1) * limit}, ${limit}`;
            }

            database.query(query, (err, rows, fields) => {
                if (err) {
                    reject({
                        status: 500,
                        message: "invalid parameters",
                    });
                }

                if (rows) {
                    if (!rows.length) {
                        reject({
                            status: 404,
                            message: "data product not found.",
                        });
                    }

                    resolve(rows);
                }
            });
        });
    },

    async createProduct(data) {
        return new Promise((resolve, reject) => {
            const queryInsert = "INSERT INTO products SET ?";

            database.query(queryInsert, data, (err, rows, fields) => {
                if (err) reject(err);
                resolve(rows);
            });
        });
    },

    async getDetailProduct(id) {
        return new Promise((resolve, reject) => {
            const query = "SELECT * FROM products WHERE id=?";

            database.query(query, id, (err, product, fields) => {
                if (err) reject(err);

                if (!product.length) {
                    reject({
                        status: 404,
                        message: "data product not found.",
                    });
                }

                resolve(product[0]);
            });
        });
    },

    async updateProduct(id, data) {
        return new Promise((resolve, reject) => {
            const querySelect = "SELECT * FROM products WHERE id=?";
            const queryUpdate = "UPDATE products SET ? WHERE id=?";

            database.query(querySelect, id, (err, products, fields) => {
                if (err) throw err;

                if (!products.length) {
                    reject({
                        status: 404,
                        message: "data product not found.",
                    });
                }

                database.query(queryUpdate, [data, id], (err, rows, fields) => {
                    if (err) throw err;
                    resolve(products[0]);
                });
            });
        });
    },

    async deleteProduct(id) {
        return new Promise((resolve, reject) => {
            const querySelect = "SELECT * FROM products WHERE id=?";
            const queryDelete = "DELETE FROM products WHERE id=?";

            database.query(querySelect, id, (err, products, fields) => {
                if (err) throw err;

                if (!products.length) {
                    reject({
                        status: 404,
                        message: "data product not found.",
                    });
                }

                database.query(queryDelete, id, (err, rows, fields) => {
                    if (err) throw err;
                    resolve(products[0]);
                });
            });
        });
    },
};

module.exports = productModel;
