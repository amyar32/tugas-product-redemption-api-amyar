const database = require("../config/database");
const { hash } = require("../utils/bcrypt");

const userModel = {
    async getAllUser(order = "id.asc", limit = 5, page = 1) {
        return new Promise((resolve, reject) => {
            let query = "SELECT * FROM users ";

            if (order) {
                const orderParam = order.split(".");
                query += `ORDER BY ${orderParam[0]} ${orderParam[1]} `;
            }

            if (page && limit) {
                query += `LIMIT ${(page - 1) * limit}, ${limit}`;
            }

            database.query(query, (err, rows, fields) => {
                if (err) {
                    reject({
                        status: 500,
                        message: "invalid parameters",
                    });
                }
                if (rows) {
                    if (!rows.length) {
                        reject({
                            status: 404,
                            message: "data user not found.",
                        });
                    }

                    resolve(rows);
                }
            });
        });
    },

    async createUser(body) {
        return new Promise(async (resolve, reject) => {
            const queryInsert = "INSERT INTO users SET ?";

            const hashedPassword = await hash(body.password);

            const data = {
                ...body,
                password: hashedPassword,
            };

            database.query(queryInsert, data, (err, rows, fields) => {
                if (err) {
                    if (err.code == "ER_DUP_ENTRY") {
                        reject({
                            status: 500,
                            message: "email has already taken",
                        });
                    }
                }
                resolve(rows);
            });
        });
    },

    async getDetailUser(id) {
        return new Promise((resolve, reject) => {
            const query = "SELECT * FROM users WHERE id=?";

            database.query(query, id, (err, user, fields) => {
                if (err) throw err;

                if (!user.length) {
                    reject({
                        status: 404,
                        message: "Data user not found.",
                    });
                }

                resolve(user[0]);
            });
        });
    },

    async getUserByEmail(email) {
        return new Promise((resolve, reject) => {
            const query = "SELECT * FROM users WHERE email=?";

            database.query(query, email, (err, user, fields) => {
                if (err) throw err;

                if (!user.length) {
                    reject({
                        status: 404,
                        message: "Data user not found.",
                    });
                }

                resolve(user[0]);
            });
        });
    },

    async updateUser(id, data) {
        return new Promise((resolve, reject) => {
            const querySelect = "SELECT * FROM users WHERE id=?";
            const queryUpdate = "UPDATE users SET ? WHERE id=?";

            database.query(querySelect, id, (err, users, fields) => {
                if (err) throw err;

                if (!users.length) {
                    reject({
                        status: 404,
                        message: "Data user not found.",
                    });
                }

                database.query(queryUpdate, [data, id], (err, rows, fields) => {
                    if (err) throw err;
                    resolve(users[0]);
                });
            });
        });
    },
    async deleteUser(id) {
        return new Promise((resolve, reject) => {
            const querySelect = "SELECT * FROM users WHERE id=?";
            const queryDelete = "DELETE FROM users WHERE id=?";

            database.query(querySelect, id, (err, users, fields) => {
                if (err) throw err;

                if (!users.length) {
                    reject({
                        status: 404,
                        message: "Data user not found.",
                    });
                }
                database.query(queryDelete, id, (err, rows, fields) => {
                    if (err) throw err;
                    resolve(users[0]);
                });
            });
        });
    },
};

module.exports = userModel;
