const productModel = require("../../../model/productModel");

const productController = {
    getAllProduct: async (req, res) => {
        try {
            const products = await productModel.getAllProduct(req.query.order, req.query.limit, req.query.page);
            res.status(200).json({
                data: products,
            });
        } catch (err) {
            res.status(err.status).json({
                message: err.message,
            });
        }
    },

    createProduct: async (req, res) => {
        const data = req.body;

        const insert = await productModel.createProduct(data);
        const product = await productModel.getDetailProduct(insert.insertId);

        res.status(201).json({
            data: product,
        });
    },

    getDetailProduct: async (req, res) => {
        const id = req.params.id;

        try {
            const product = await productModel.getDetailProduct(id);
            res.status(201).json({
                data: product,
            });
        } catch (err) {
            res.status(err.status).json({
                message: err.message,
            });
        }
    },

    updateProduct: async (req, res) => {
        const id = req.params.id;
        const data = req.body;

        try {
            const product = await productModel.updateProduct(id, data);
            res.status(201).json({
                data: product,
            });
        } catch (err) {
            res.status(err.status).json({
                message: err.message,
            });
        }
    },

    deleteProduct: async (req, res) => {
        const id = req.params.id;

        try {
            const product = await productModel.deleteProduct(id);
            res.status(201).json({
                data: product,
            });
        } catch (err) {
            res.status(err.status).json({
                message: err.message,
            });
        }
    },
};

module.exports = productController;
