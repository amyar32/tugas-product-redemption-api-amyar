const { body } = require("express-validator");

const requiredMessage = "this field is required";

module.exports = () => {
    return [
        body("name")
            .notEmpty()
            .withMessage(requiredMessage)
            .isLength({ max: 30 })
            .withMessage("maximum length of string is 30"),
        body("description")
            .notEmpty()
            .withMessage(requiredMessage)
            .isLength({ max: 255 })
            .withMessage("maximum length of text is 255"),
        body("point")
            .notEmpty()
            .withMessage(requiredMessage)
            .isFloat({ min: 10000 })
            .withMessage("minimal amount of point is 10000"),
        body("stock")
            .notEmpty()
            .withMessage(requiredMessage)
            .isFloat({ min: 0 })
            .withMessage("minimal amount of stock is 0"),
        body("image_url")
            .notEmpty()
            .withMessage(requiredMessage)
            .isURL()
            .withMessage("this field must be an url"),
    ];
};
