const jwt = require("jsonwebtoken");
const userModel = require("../../../model/userModel");
const { compare } = require("../../../utils/bcrypt");

const authController = {
    login: async (req, res) => {
        const email = req.body.email;
        const password = req.body.password;

        try {
            const user = await userModel.getUserByEmail(email);

            if (await compare(password, user.password)) {
                const token = jwt.sign({ email: user.email }, "secret", { expiresIn: "1h" });
                res.status(200).json({
                    name: user.name,
                    email: user.email,
                    token,
                });
            } else {
                res.status(401).json({
                    message: "invalid login information",
                });
            }
        } catch (err) {
            res.status(err.status).json({
                message: err.message,
            });
        }
    },
};

module.exports = authController;
