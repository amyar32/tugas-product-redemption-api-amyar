const { body } = require("express-validator");

const requiredMessage = "this field is required";

module.exports = () => {
    return [
        body("name").notEmpty().withMessage(requiredMessage),
        body("email").notEmpty().withMessage(requiredMessage).isEmail().withMessage("email not valid"),
        body("password")
            .notEmpty()
            .withMessage(requiredMessage)
            .isLength({ min: 5 })
            .withMessage("minimal password length is 5"),
        body("point")
            .notEmpty()
            .withMessage(requiredMessage)
            .isFloat({ min: 0 })
            .withMessage("minimal amount of point is 0"),
    ];
};
