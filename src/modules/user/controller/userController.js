const userModel = require("../../../model/userModel");

const userController = {
    getAllUser: async (req, res) => {
        try {
            const users = await userModel.getAllUser(req.query.order, req.query.limit, req.query.page);
            res.status(200).json({
                data: users,
            });
        } catch (err) {
            console.log(err);
        }
    },

    createUser: async (req, res) => {
        const data = req.body;

        try {
            const insert = await userModel.createUser(data);
            console.log(insert);
            const user = await userModel.getDetailUser(insert.insertId);
            res.status(201).json({
                data: user,
            });
        } catch (err) {
            res.status(500).json({
                message: err.message,
            });
        }
    },

    getDetailUser: async (req, res) => {
        const id = req.params.id;

        try {
            const user = await userModel.getDetailUser(id);
            res.status(201).json({
                data: user,
            });
        } catch (err) {
            res.status(err.status).json({
                message: err.message,
            });
        }
    },

    updateUser: async (req, res) => {
        const id = req.params.id;
        const data = req.body;

        try {
            const user = await userModel.updateUser(id, data);
            res.status(201).json({
                data: user,
            });
        } catch (err) {
            res.status(err.status).json({
                message: err.message,
            });
        }
    },

    deleteUser: async (req, res) => {
        const id = req.params.id;

        try {
            const user = await userModel.deleteUser(id);
            res.status(201).json({
                data: user,
            });
        } catch (err) {
            res.status(err.status).json({
                message: err.message,
            });
        }
    },
};

module.exports = userController;
