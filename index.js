const express = require("express");
const app = express();
const port = 3000;

// Init Config
const bodyParser = require("./src/config/body-parser");

// Init Route
const authRoute = require("./src/routes/authRoute");
const productRoute = require("./src/routes/productRoute");
const userRoute = require("./src/routes/userRoute");

// Config Express
app.use(bodyParser);

// Config Route
app.use(authRoute);
app.use("/product", productRoute);
app.use("/user", userRoute);

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});
